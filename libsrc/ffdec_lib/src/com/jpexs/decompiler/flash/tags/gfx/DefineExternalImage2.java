/*
 *  Copyright (C) 2010-2018 JPEXS, All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package com.jpexs.decompiler.flash.tags.gfx;

import com.jpexs.decompiler.flash.SWF;
import com.jpexs.decompiler.flash.SWFInputStream;
import com.jpexs.decompiler.flash.SWFOutputStream;
import com.jpexs.decompiler.flash.tags.base.ImageTag;
import com.jpexs.decompiler.flash.tags.enums.ImageFormat;
import com.jpexs.helpers.ByteArrayRange;
import com.jpexs.helpers.SerializableImage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author JPEXS
 */
public class DefineExternalImage2 extends ImageTag {

    public static final int ID = 1009;

    public static final String NAME = "DefineExternalImage2";

    public int bitmapFormat;

    public int targetWidth;

    public int targetHeight;

    public String exportName;

    public String fileName;

    public byte[] extraData; //?

    public static final int BITMAP_FORMAT_DEFAULT = 0;

    public static final int BITMAP_FORMAT_TGA = 1;

    public static final int BITMAP_FORMAT_DDS = 2;

    private SWF swf;

    private BufferedImage uncompressedImage;

    /**
     * Gets data bytes
     *
     * @param sos SWF output stream
     * @throws java.io.IOException
     */
    @Override
    public void getData(SWFOutputStream sos) throws IOException {
        sos.writeUI32(characterID);
        sos.writeUI16(bitmapFormat);
        sos.writeUI16(targetWidth);
        sos.writeUI16(targetHeight);
        sos.writeNetString(exportName);
        sos.writeNetString(fileName);
        if (extraData != null) {
            sos.write(extraData);
        }
    }

    /**
     * Constructor
     *
     * @param sis
     * @param data
     * @throws IOException
     */
    public DefineExternalImage2(SWFInputStream sis, ByteArrayRange data) throws IOException {
        super(sis.getSwf(), ID, NAME, data);
        readData(sis, data, 0, false, false, false);
    }

    @Override
    public final void readData(SWFInputStream sis, ByteArrayRange data, int level, boolean parallel, boolean skipUnusualTags, boolean lazy) throws IOException {
        characterID = (int) sis.readUI32("characterId");
        bitmapFormat = sis.readUI16("bitmapFormat");
        targetWidth = sis.readUI16("targetWidth");
        targetHeight = sis.readUI16("targetHeight");
        exportName = sis.readNetString("exportName");
        fileName = sis.readNetString("fileName");
        if (sis.available() > 0) { //there is usually one zero byte, bod knows why
            extraData = sis.readBytesEx(sis.available(), "extraData");
        }
        swf = sis.getSwf();
        uncompress();
    }

    private void uncompress() throws IOException {
        File file = new File(new File(swf.getFile()).getParent(), "Textures/" + fileName);
        if (!file.exists()) {
            System.err.println("Can't open texture " + fileName);
            return;
        }
        uncompressedImage = ImageIO.read(file);
        if (targetWidth != uncompressedImage.getWidth() || targetHeight != uncompressedImage.getHeight()) {
            BufferedImage resizedImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_ARGB);
            Graphics g = resizedImage.getGraphics();
            g.drawImage(uncompressedImage, 0, 0, targetWidth, targetHeight, null);
            g.dispose();
            uncompressedImage = resizedImage;
        }
    }

    @Override
    public InputStream getOriginalImageData() {
        return null;
    }

    @Override
    protected SerializableImage getImage() {
        return new SerializableImage(uncompressedImage);
    }

    @Override
    public Dimension getImageDimension() {
        return new Dimension(targetWidth, targetHeight);
    }

    @Override
    public void setImage(byte[] data) {}

    @Override
    public ImageFormat getImageFormat() {
        return ImageFormat.PNG;
    }

    @Override
    public ImageFormat getOriginalImageFormat() {
        return ImageFormat.PNG;
    }
}
